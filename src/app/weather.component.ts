import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ApixuService } from './apixu.service';

@Component({
  selector: 'app-weather',
  templateUrl: 'weather.component.html',

})
export class WeatherComponent implements OnInit{
  public submited: boolean;
  public weatherForm: FormGroup;
  public weatherData: any;

  constructor(private formBuilder: FormBuilder, private apixuService: ApixuService) {}

  ngOnInit() {
    this.weatherForm = this.formBuilder.group({
      location: ['']
    });
  }

  sendReq(formValues) {
    this.apixuService.getWeather(formValues.location).subscribe(data => {this.weatherData = data
      console.log(this.weatherData);});
  }
}
