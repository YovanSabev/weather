import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApixuService {

  constructor(private http: HttpClient) {}

  getWeather(location){
      return this.http.get(
          'http://api.weatherstack.com/current?access_key=1028c35e20d59a0543b8a208148cc33e&query=' + location
      );
  }
}
